from django.db import models

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, default="")


class Hat(models.Model):
    fabric = models.CharField(max_length=20)
    style_name = models.CharField(max_length=50)
    color = models.CharField(max_length=20)
    picture_url = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="Hats",
        on_delete=models.CASCADE,
        default=""

    )

    def __str__(self):
        return self.style_name
