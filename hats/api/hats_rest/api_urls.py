from django.urls import path

from .api_views import api_show_hat, api_list_hats


urlpatterns = [
    path("hats/", api_list_hats, name="api_list_hats"),
    path("hats/<int:pk>/", api_show_hat, name="api_list_hats"),
]
