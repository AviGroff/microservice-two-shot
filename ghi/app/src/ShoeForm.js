import React, {useEffect, useState} from 'react';


function ShoeForm (props) {
    const [bins, setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState('');
    const [model, setModel] = useState('');
    const [color, setColor] = useState('');
    const [bin, setBin] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.manufacturer = manufacturer;
        data.model = model;
        data.color = color;
        data.bin = bin;

        console.log(data);

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);
            setManufacturer('');
            setModel('');
            setColor('');
            setBin('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

    const handleManufacturerChange = (event) => {

        const value = event.target.value;
        setManufacturer(value);
      }

    const handleModelChange = (event) => {

        const value = event.target.value;
        setModel(value);
    }

    const handleColorChange = (event) => {

        const value = event.target.value;
        setColor(value);
    }

    const handleBinChange = (event) => {

        const value = event.target.value;
        setBin(value);
    }


    useEffect(() => {
        fetchData();
    }, []);

    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none'; // what could go here?
    if (bins.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';// what should be here?
        dropdownClasses = 'form-select';// what should be here?
    }

        return (
        <div className="my-5 container">
            <div className="row">
                <div className="col col-sm-auto">
                <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src={require("./wardrobify.jpg")}/>
                </div>
                <div className="col">
                <div className="card shadow">
                    <div className="card-body">
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <h1 className="card-title">It's Shoe Time!</h1>
                        <p className="mb-3">
                        Please choose which bin
                        you'd like to put your shoes in.
                        </p>
                        <div className= {spinnerClasses} id="loading-conference-spinner">
                        <div className="spinner-grow text-secondary" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                        </div>
                        <div className="mb-3">
                        <select value = {bin} onChange = {handleBinChange} name="bin" id="bin" className={dropdownClasses} required>
                            <option value="">Choose a bin</option>
                            {bins.map(bin => {
                            return (
                            <option value = {bin.href} key = {bin.href}>
                            {bin.id}
                            </option>
                                );
                            })}
                        </select>
                        </div>
                        <p className="mb-3">
                        Now, tell us about your shoes.
                        </p>
                        <div className="row">
                        <div className="col">
                            <div className="form-floating mb-3">
                            <input value = {manufacturer} onChange = {handleManufacturerChange} required placeholder="manufacturer" type="text" id="name" name="name" className="form-control"/>
                            <label htmlform="name">Manufacturer</label>
                            </div>
                        </div>
                        <div className="col">
                            <div className="form-floating mb-3">
                            <input value = {model} onChange = {handleModelChange} required placeholder="model" type="text" id="name" name="name" className="form-control"/>
                            <label htmlform="name">Model</label>
                            </div>
                        </div>
                        <div className="col">
                            <div className="form-floating mb-3">
                            <input value = {color} onChange = {handleColorChange} required placeholder="color" type="text" id="name" name="name" className="form-control"/>
                            <label htmlform="name">Color</label>
                            </div>
                        </div>
                        </div>
                        <button className="btn btn-lg btn-primary">Add!</button>
                    </form>
                    <div className="alert alert-success d-none mb-0" id="success-message">
                        Congratulations! You're all signed up!
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
            );
}
export default ShoeForm;
