import { Link } from 'react-router-dom';

const handleSubmit = async (event) => {
  event.preventDefault();

  const hatId = event.target.id;

  const hatUrl = `http://localhost:8090/api/hats/${hatId}`;

  const response = await fetch(hatUrl, {method: "delete", headers: {
    'Content-Type': 'application/json',
    }});
  if (response.ok) {
      const confirmation = await response.json();
      console.log(confirmation);
      window.location.reload(false);
  }
}


function HatList(props) {
    const Hats = props.hats
    return (
      <div className="col">
        {Hats.map(hat => {
        return (
            <div key={hat.id} className="card mb-3 shadow">
                <img src={hat.picture_url} className="card-img-top"/>
                <div className="card-body">
                    <h5 className="card-title">{hat.fabric}</h5>
                    <p className="card-text">{hat.style_name}</p>
                    <p className="card-text">{hat.color}</p>
                    <form onSubmit={handleSubmit} id={hat.id}>
                    <div className="d-grid gap-2 d-sm-flex justify-content-sm-right">
                    <button value={hat.id} className="btn btn-danger">Delete</button>
                    </div>
                    </form>
                </div>
            </div>
        );
      })}
        </div>
        );

      }

export default HatList;
