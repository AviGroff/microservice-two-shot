import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


async function loadData() {
  const shoeResponse = await fetch('http://localhost:8080/api/shoes/');
  const hatResponse = await fetch('http://localhost:8090/api/hats/');
  console.log(shoeResponse);
  // this if should also include hatResponse.ok
  // we'll do 2 seperate propertires of app shoes = and hats =
  if (shoeResponse.ok && hatResponse.ok){
    const data = await shoeResponse.json();
    const hatData = await hatResponse.json();
    console.log(data.shoes);
    root.render(
      <React.StrictMode>
        <App shoes={data.shoes} hats={data.hats} />
      </React.StrictMode>
    );
  } else {
    console.log("error");
  }
}
loadData();
