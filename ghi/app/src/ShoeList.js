// function createCard(manufacturer, model, pictureUrl, color) {
//     return (
//       <div className="card">
//         <img src={pictureUrl} className="card-img-top"/>
//         <div className="card-body">
//           <h5 className="card-title">{manufacturer}</h5>
//           <p className="card-text">{model}</p>
//           <p className="card-text">{color}</p>
//         </div>
//       </div>
//     )
//   }

import { Link } from 'react-router-dom';

// const handleSubmit = async (event) => {
//   event.preventDefault();

//   const shoeId = event.target.id;
//   console.log("shoeid " + shoeId)

//   const shoeUrl = `http://localhost:8080/api/shoes/${shoeId}`;

//   const response = await fetch(shoeUrl, {method: "delete", headers: {
//     'Content-Type': 'application/json',
//     }});
//   if (response.ok) {
//       const confirmation = await response.json();
//       console.log(confirmation);
//       window.location.reload(false);
//   }
// }


function ShoeList(props) {

  const handleSubmit = async (event) => {
    event.preventDefault();

    const shoeId = event.target.id;
    console.log("shoeid " + shoeId)

    const shoeUrl = `http://localhost:8080/api/shoes/${shoeId}`;

    const response = await fetch(shoeUrl, {method: "delete", headers: {
      'Content-Type': 'application/json',
      }});
    if (response.ok) {
        const confirmation = await response.json();
        console.log(confirmation);
        window.location.reload(false);
    }
  }

    const shoes = props.shoes
    console.log("yo hwats up")
    return (
      <div className="col">
        {shoes.map(shoe => {
        return (
            <div key={shoe.id} className="card mb-3 shadow">
                <img src={shoe.picture_url} className="card-img-top"/>
                <div className="card-body">
                    <h5 className="card-title">{shoe.manufacturer}</h5>
                    <p className="card-text">{shoe.model}</p>
                    <p className="card-text">{shoe.color}</p>
                    <form onSubmit={handleSubmit} id={shoe.id}>
                    <div className="d-grid gap-2 d-sm-flex justify-content-sm-right">
                    <button value={shoe.id} className="btn btn-danger">Delete</button>
                    </div>
                    </form>
                </div>
            </div>
        );
      })}
        </div>
        );

      }

export default ShoeList;
