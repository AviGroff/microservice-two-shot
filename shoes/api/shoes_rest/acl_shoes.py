import requests


PEXELS_API_KEY = "4BF6ElSOWNDNfNkINFJY7sUZ8kUh4stAH2fjsvawQ1nRfF1VcRWKHtI0"

def get_shoe_photo(manufacturer):
    headers = {"Authorization": PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query={manufacturer}"
    resp = requests.get(url, headers=headers)
    try:
        return resp.json()["photos"][0]["src"]["original"]
    except (KeyError, IndexError):
        return 'https://t4.ftcdn.net/jpg/00/89/55/15/360_F_89551596_LdHAZRwz3i4EM4J0NHNHy2hEUYDfXc0j.jpg'
