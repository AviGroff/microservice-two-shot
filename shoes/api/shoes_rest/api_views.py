from .models import Shoe, BinVO
from .acl_shoes import get_shoe_photo
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href"]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "model", "color", "bin", "picture_url", "id"]

    encoders = {"bin": BinVODetailEncoder()}
class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "model", "color", "bin", "picture_url"]

    encoders = {"bin": BinVODetailEncoder()}

@require_http_methods(["GET", "POST"])
def api_list_shoes(request):

    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the Location object and put it in the content dict
        try:
            bin = BinVO.objects.get(import_href=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin href"},
                status=400,
            )

        content["picture_url"] = get_shoe_photo(content["manufacturer"])

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_shoe(request, id):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=id)

        return JsonResponse(
            {"shoe": shoe},
            encoder=ShoeDetailEncoder,
            safe=False,
        )

    elif request.method == "PUT":
        content = json.loads(request.body)

        # new code
        Shoe.objects.filter(id=id).update(**content)
        # copied from get detail
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
