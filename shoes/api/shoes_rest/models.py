from django.db import models

# Create your models here.
class BinVO(models.Model):
    # bin_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)
class Shoe(models.Model):
    manufacturer = models.CharField(max_length=20)
    model = models.CharField(max_length=20)
    color = models.CharField(max_length=20)

    bin = models.ForeignKey(
        BinVO,
        related_name = "shoe",
        on_delete=models.CASCADE,
        default = ""
    )

    picture_url = models.URLField(default="")
